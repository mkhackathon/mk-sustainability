# Milton Keynes Sustainability information site

## Development notes

### Hosting

Successfully built pushes to `main` are deployed to [here](https://clever-colden-41547a.netlify.app/).

### Commit policy

We don't have vetted pull request, but please run `npm build` before pushing anything commit to `develop`, making sure that it builds cleanly.

### To start developing

1.  **Start developing.**

    In the project root

    `npm install`

    to download the dependencies (you may need it every time you get new commits from the repository), and

    `npm run develop`

    to start the development server

1.  **Open the source code and start editing!**

    Your site is now running at `http://localhost:8000`!

    _Note: You'll also see a second link: _`http://localhost:8000/___graphql`_. This is a tool you can use to experiment with querying your data. Learn more about using this tool in the [Gatsby tutorial](https://www.gatsbyjs.com/tutorial/part-five/#introducing-graphiql)._

    Open the root directory in your code editor of choice and edit `src/pages/index.js`. Save your changes and the browser will update in real time!

### Learning Gatsby

Looking for more guidance? Full documentation for Gatsby lives [on the website](https://www.gatsbyjs.com/). Here are some places to start:

- **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.com/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.com/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.
