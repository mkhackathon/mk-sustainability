* About Us
* What is sustainability
* Transport
  * short explanation and easy steps
  * walking
  * cycling
  * bikes/scooters
  * bus
  * other transport options (train, electric car)
* Retail
  * short explanation and easy step
  * shop second hand
  * local/independent
* Homes (Meredith)
 * quick explanation
 * green homes
 * green energy
* Food
  * quick explanation, easy steps
  * low and zero waste
  * community fridges
  * food sharing
  * online resources
* Business
  * easy steps and quick explanation
* Conservation/local environment
  * information and easy steps
  * ways to get involved
* Community groups
  * info and ways to get involved
* Waste
  * reduce
  * reuse
  * upcycling
  * reusable items
  * recycle - done