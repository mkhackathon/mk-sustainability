import React from "react"

import Layout from "../components/layouts/default"
import Locations from "../components/locations"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Locations />
  </Layout>
)

export default IndexPage
