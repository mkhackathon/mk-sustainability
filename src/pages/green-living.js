import * as React from "react"

import Layout from "../components/layouts/green-living"
import SEO from "../components/seo"

const IndexPage = () => {
  return (
    <Layout>
      <SEO title="Green Living" />
      <h1>Green living</h1>
    </Layout>
  )
}

export default IndexPage
