import React from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/layouts/green-living"
import { MDXRenderer } from "gatsby-plugin-mdx"
import kebabCase from "lodash/kebabCase"

export const query = graphql`
  query($slug: String!) {
    mdx(fields: { collection: { eq: "green-living" } }, slug: { eq: $slug }) {
      slug
      body
      frontmatter {
        title
        url
        tags
      }
    }
  }
`

const GreenLivingTemplate = ({ data: { mdx: glPage } }) => (
  <Layout>
    <h1>{glPage.frontmatter.title}</h1>
    <div className="tags-container">
    <ul className="tag-list">
      {glPage.frontmatter.tags.map(t => (
        <li key={t + '-tag'}>
        <Link to={`/tags/${kebabCase(t)}`}>{t}</Link>
        </li>
      ))}
      </ul>
    </div>
    <MDXRenderer>{glPage.body}</MDXRenderer>
  </Layout>
)

export default GreenLivingTemplate
