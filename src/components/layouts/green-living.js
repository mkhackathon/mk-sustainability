import * as React from "react"
import useGreenLiving from "../../hooks/use-green-living"
import DefaultLayout from "./default"
import { Link } from "gatsby"

const Layout = ({ children }) => {
  const glPages = useGreenLiving()

  return (
    <DefaultLayout>
      {children}

      <ul>
      {glPages.map(gl => (
        <li>
        <Link to={`/green-living/${gl.slug}`}>{gl.title}</Link>
        </li>
      ))}
      </ul>

    </DefaultLayout>
  )
}

export default Layout
