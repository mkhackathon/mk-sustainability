import FoodSharing from "../images/map/icon-food.svg"
import CommunityGroup from "../images/map/icon-groups.svg"
import RecyclingCentre from "../images/map/icon-recycle.svg"
import SustainableShop from "../images/map/icon-shop.svg"
import RecyclingPoint from "../images/map/icon-points.svg"
import StarBlack from "../images/map/star_b_trans.png"

const icons = {
  FoodSharing: FoodSharing,
  CommunityGroup: CommunityGroup,
  RecyclingCentre: RecyclingCentre,
  SustainableShop: SustainableShop,
  RecyclingPoint: RecyclingPoint,
  StarBlack: StarBlack,
}

const categories = [
  {
    id: "sustainable-shop",
    csvName: "Shop Sustainable",
    display: "Shop Sustainable",
    icon: icons.SustainableShop,
    color: "#FA4C35",
  },
  {
    id: "recycling-centre",
    csvName: "Recycling Centre",
    display: "Recycling Centres",
    icon: icons.RecyclingCentre,
    color: "#4BAF4C",
  },
  {
    id: "recycling-point",
    csvName: "Recycling Points",
    display: "Recycling Points",
    icon: icons.RecyclingPoint,
    color: "#0035BA",
  },
  {
    id: "community-group",
    csvName: "Community Groups",
    display: "Community Groups",
    icon: icons.CommunityGroup,
    color: "#C48E00",
  },
  {
    id: "food-sharing",
    csvName: "Food Sharing",
    display: "Food Sharing",
    icon: icons.FoodSharing,
    color: "#01A498",
  },
]

export default categories
