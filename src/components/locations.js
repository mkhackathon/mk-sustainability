import styled from "@emotion/styled"
import { Link } from "gatsby"
import React, { useState } from "react"
import Map, { MapRoot } from "./map"
import categories from "./categories"
import { useHasMounted } from "../hooks/useHasMounted"
import SearchIcon from '../images/icon-search.svg'

const CategorySelectorWrapper = styled.label`
  height: 54px;

  border-radius: 32px;
  border: 3px solid transparent;

  display: inline;

  &.checked {
    border: 3px solid ${({ color }) => (color ? color : "black")};
  }

  position: relative;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;

  margin-left: -6px;

  font-weight: 700;
  color: ${({ color }) => (color ? color : "black")};

  input {
    display: block;
    position: absolute;
    width: 100%;
    height: 100%;
    opacity: 0;
  }

  img {
    margin: 3px 0 3px 3px;
  }

  img + span {
    margin-left: 8px;
  }
`

const CategorySelector = ({ icon, children, color, checked, setChecked }) => {
  return (
    <CategorySelectorWrapper
      className={checked ? "checked" : "non-checked"}
      color={color}
    >
      <input
        type="checkbox"
        checked={checked}
        onChange={e => setChecked(e.target.checked)}
      />
      <img src={icon} role="presentation" alt="" />
      <span>{children}</span>
    </CategorySelectorWrapper>
  )
}

const LocationsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  overflow: hidden;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;

  font-size: 1.125rem;

  .map-menu {
    width: 390px;
    height: 100%;
    overflow-y: auto;
    background-color: white;

    padding: 2rem 30px 1rem 30px;

    display: flex;
    flex-direction: column;

    h2 {
      font-family: Faune, Arial, sans-serif;
      margin-bottom: 1rem;
    }

    ${CategorySelectorWrapper} {
      margin-top: 10px;
    }

    p {
      margin-top: 1.5rem;
    }
  }

  .map-and-search {
    flex-grow: 1;
    flex-shrink: 1;
    height: 100%;
    width: 100%;

    position: relative;

    ${MapRoot} {
      height: 100%;
      width: 100%;
    }

    .search {
      z-index: 1500;
      position: absolute;


      width: 360px;
      left: 0.5rem;
      top: 0.5rem;
      border-radius: 32px;

      input[type=text], .search-info {
      }

      input[type=text] {
        border-radius: 32px;
        color: #0045ed;
        border-color: #0045ed;
        border-style: solid;
        border-width: 3px;
        font-size: 1.3125rem;

        outline: none;

        width: 100%;
        padding-left: 0.5em;
        padding-right: calc(8px + 44px);
        padding-top: 0.5em;
        padding-bottom: 0.525em;
      }

      .search-info {
        display: block;
        z-index: -1;
        background-color: white;

        margin-top: -1.3125rem;
        padding: 1.5rem 1rem 0.75rem 1rem;
        border-radius: 0 0 20px 20px;
        border: solid 4px #cdcdcd;

        .clear-search {
          display: block;
        }
      }

      .search-button {
        background-color: transparent;
        border: none;
        cursor: pointer;
        position: absolute;
        top: 0.25em;
        right: 1px;
      }
    }
  }

`

const defaultSelection = [
  ...categories.map(cat => ({
    category: cat,
    selected: true,
  })),
]

const Locations = () => {

  const hasMounted = useHasMounted()
  const [selection, setSelection] = useState(() => defaultSelection)

  const changeSelection = (id, value) => {
    setSelection(old =>
      old.map(sel =>
        sel.category.id !== id ? sel : { ...sel, selected: value }
      )
    )
  }

  const selectedCategories = selection
    .filter(sel => sel.selected)
    .map(sel => sel.category)

  const resetCategories = () => { setSelection(defaultSelection) }

  const [searchText, setSearchText] = useState("")
  const [search, setSearch] = useState({ term: null })

  const runSearch = (term) => {
    setSearchText(term)
    setSearch({term: term !== "" ? term : null})
    resetCategories()
  }

  return (
    <LocationsWrapper>
      <div className="map-and-search">
        <div className="search">
          <input name="search"
                 type="text"
                 value={searchText}
                 onChange={e => setSearchText(e.target.value)}
                 onKeyPress={e => { if (e.key === "Enter") { runSearch(searchText) } }} >
          </input>
          <button className="search-button" onClick={() => runSearch(searchText)}>
            <img src={SearchIcon} alt="footer logo" />
          </button>
          <div className="search-info">
            {search.term === null
             ? <p>Search for materials you would like to recycle, a place, a category or a tag</p>
             : <>
                 <button className="clear-search link" onClick={ () => runSearch("") }>Clear your search</button>
                 <span className="search-term">{search.term}</span>
               </>
            }
          </div>
        </div>
        {hasMounted ? <Map selectedCategories={selectedCategories} searchTerm={search.term} /> : <div />}
      </div>
      <div className="map-menu">
        <h2>What would you like to see?</h2>
        {selection.map(
          ({ category: { id, display, icon, color }, selected }) => (
            <CategorySelector
              key={id}
              icon={icon}
              color={color}
              checked={selected}
              setChecked={value => changeSelection(id, value)}
            >
              {display}
            </CategorySelector>
          )
        )}
        <p>
          Can't find what you're looking for? Why not have a look on our{" "}
          <Link to="/green-living">Green Living</Link> page
        </p>
      </div>
    </LocationsWrapper>
  )
}

export default Locations
