export const words = s => (s ?? "").split(/[, ]+/).map(t => t.trim().toLowerCase()).filter(s => s.length > 0)
