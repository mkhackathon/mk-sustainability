import { graphql, useStaticQuery } from "gatsby"
import { words } from '../utils/text'

const useLocations = () => {
  const data = useStaticQuery(graphql`
    query {
      allLocationsCsv {
        nodes {
          lat_
          long_
          link
          title_
          address_
          postcode_
          googlemaps_url
          category_
          material_tags_
          description
          image
          additional_information
        }
      }
    }
  `)

  return data.allLocationsCsv.nodes.map((l,idx) => ({
    idx: idx,
    lat: l.lat_,
    long: l.long_,
    link: l.link,
    title: l.title_,
    words: new Set([...words(l.title_), ...words(l.address_), ...words(l.material_tags_)]),
    address: l.address_,
    addressWords: new Set(words(l.address_.replace(",", " "))),
    postcode: l.postcode_,
    googlemapsUrl: l.googlemaps_url,
    category: (l.category_ ?? "").split(",")[0].trim(),
    materialTags: l.material_tags_.split(',').map(t => t.trim()).filter(t => t.length > 0),
    description: l.description,
    image: l.image,
    additionalInformation: l.additional_information,
  }))
}

export default useLocations
